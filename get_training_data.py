import re
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import h5py
import animateLightField as animlf
import os
from normalize import normalize
import hinton
import weighted_loss as wl
from flags_config import Flags
from flags_config import Config
import lmdb
import numpy as np
import random
from random import randint
random.seed(20)

patchSize = Config.patchSize

N = Config.N
global_mask = np.zeros((N,patchSize[2],patchSize[3]), dtype='float32')
for l in range(N):
    for i in range(patchSize[2]):
        for j in range(patchSize[3]):
            global_mask[l,i,j] = np.double(randint(0,1))

def shift_down(input_,shift):
    if (shift ==0): return input_
    matrix = np.zeros((input_.shape))
    size = len(matrix)
    shift = shift% patchSize[3]
    for i in range(size):
        matrix[:,i] = np.append(input_[shift:,i],input_[:shift,i])
    return matrix

def shift_right(input_,shift):
    if (shift==0): return input_
    matrix = np.zeros((input_.shape))
    size = len(matrix)
    for i in range(size):
        matrix[i,:] = np.append(input_[i,shift:],input_[i,:shift])
    return matrix

def mask_phi(x,y):
    n_rows = N*patchSize[0]*patchSize[1]
    n_columns = patchSize[0]*patchSize[1]*patchSize[2]*patchSize[3]
    output_ = np.zeros((n_rows,n_columns))
    mask_template = np.zeros((global_mask.shape))
    for k in range(N):
        mask_template[k,:,:] = shift_down(shift_right(global_mask[k,:,:],y),x)
    for i in range(patchSize[0]):
        for j in range(patchSize[1]):
            for k in range(N):
                output_[(i*patchSize[1]+j)*N+k , (i*patchSize[1]+j)*patchSize[2]*patchSize[3]:(i*patchSize[1]+j)*patchSize[2]*patchSize[3]+patchSize[2]*patchSize[3]] = np.ravel(shift_down(shift_right(mask_template[k,:,:],j),i))
    return output_

#def get_masked_measurement(x,y,patch):
#    output = 
#    for l in range(N):
#        output = np.zeros((patchSize[0],patchSize[1]))
#        template = shift_down(shift_right(global_mask[l],x),y)
#        for i in range(patchSize[0]):
#            for j in range(patchSize[1]):
#                shifted_template = shift_down(shift_right(template,i),j)
#                output[i,j] = np.sum(patch[i,j,:,:]*shifted_template)
#        return output

def load_lf_to_tensor_syn(filename):
  #  trainingLightFieldFilenames = ['fishi_5x5_ap35','messerschmitt_5x5_ap35','shrubbery_5x5_ap35', 'dice_5x5_ap35',
    #trainingLightFieldFilenames = ['messerschmitt_5x5_ap35']
    # number of training datasets
   # numTrainingLightFields = len(trainingLightFieldFilenames)
    #numTrainingLightFields = 1 
    lightFieldRes = Config.lightFieldRes

    # Preallocate TT
    TT = np.zeros((1,1)+lightFieldRes)

    #load all 4D training light fields
    #Path = os.path.dirname(os.path.realpath(__file__)) + '/sig_16_data/TrainingSet/Data/'  #'/../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'
    Path = os.path.dirname(os.path.realpath(__file__)) + '/../CompressiveLightFieldPhotography-DatasetsAndCode/SyntheticExperiments/DictionaryLearning/'

    #for k in range(0,numTrainingLightFields):
    #shortname = re.search('(.+?)_',trainingLightFieldFilenames[k]).group(1)
    #shortname = re.search('(.+?).png',filename).group(1)
    shortname = re.search('(.+?)_', os.path.basename(filename)).group(1)
    print "Loading: ", shortname

    for ky in range(lightFieldRes[2]):
        for kx in range(lightFieldRes[3]):


            # Read image in grayscale
            #I = cv2.imread(Path + shortname + '_' + '%d' % (ky*lightFieldRes[2]+kx+1) + '.png',0)
            I = cv2.imread(Path + filename + '/' + shortname + '-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png', 0)
            #print Path + filename + '/' + shortname + '-' + '%02d' % (ky*lightFieldRes[2]+kx+1) + '.png'
            I = I.astype('float64')
            #I = I-I.min()
            #I = I/(I.max()-I.min())
            I = I/256.0
            # Store values, [LF_indx,nColor,spatial_y,spatial_x,angular_y,angular_x]
            TT[0,0,:,:,ky,kx] = I[:,:]  # some of the images are of size 375,540
    ## animate
    if Flags.animate == 1:
        # Changing the shape of TT to expected animated shape
        # expected [angular_y angular_x spatial_y spatial_x numColorChannels]
        # Just animating the first(fishy) data
        lf = np.transpose(TT[0,:,:,:,:,:],(3,4,1,2,0));
        print 'Animating the input: Close the figure to proceed'
        animlf.animateLightField(lf)
    return [TT, 1]

def create_training_data(formPhi_func, phitype, mask_phi_flag==1):
    '''
    Generates the training and validation patches for the light-field.
    formphi_func : specify the type of phi matrix to be used (synthetic or real), for Synthetic mask phi just set the flag to be 1.
    phiType: To specify the Real or Synthetic phi in the data files thus generated.  
    '''

    # unpack data_in
    #trainingLightFieldFilenames = [ 'bikes_11_eslf.png',  'bikes_13_eslf.png',  'bikes_4_eslf.png',  'buildings_10_eslf.png' , 'buildings_6_eslf.png' , 'cars_36_eslf.png', 'cars_38_eslf.png' , 'cars_44_eslf.png' , 'flowers_plants_17_eslf.png' , 'flowers_plants_24_eslf.png' , 'flowers_plants_42_eslf.png' , 'general_15_eslf.png' , 'general_31_eslf.png' , 'general_9_eslf.png','bikes_12_eslf.png' , 'bikes_20_eslf.png' , 'bikes_9_eslf.png' , 'buildings_3_eslf.png' , 'cars_21_eslf.png' , 'cars_37_eslf.png' , 'cars_39_eslf.png' , 'cars_50_eslf.png' , 'flowers_plants_23_eslf.png' , 'flowers_plants_28_eslf.png' , 'flowers_plants_62_eslf.png' , 'general_19_eslf.png' , 'general_4_eslf.png' ,  'occlusions_24_eslf.png', 'IMG_0288_eslf.png','IMG_0466_eslf.png', 'IMG_0681_eslf.png',  'IMG_1410_eslf.png',  'IMG_1416_eslf.png',  'IMG_1471_eslf.png',  'IMG_1476_eslf.png', 'IMG_1480_eslf.png',  'IMG_1484_eslf.png',  'IMG_1499_eslf.png',  'IMG_1505_eslf.png',  'IMG_1511_eslf.png',  'IMG_1522_eslf.png',  'IMG_1530_eslf.png', 'IMG_1546_eslf.png',  'IMG_1566_eslf.png' , 'IMG_1582_eslf.png',  'IMG_1598_eslf.png', 'IMG_0289_eslf.png',  'IMG_0518_eslf.png',  'IMG_0780_eslf.png',  'IMG_1413_eslf.png',  'IMG_1419_eslf.png',  'IMG_1473_eslf.png', 'IMG_1477_eslf.png','IMG_1481_eslf.png',  'IMG_1486_eslf.png',  'IMG_1500_eslf.png',  'IMG_1508_eslf.png', 'IMG_1513_eslf.png', 'IMG_1523_eslf.png',  'IMG_1534_eslf.png',  'IMG_1547_eslf.png',  'IMG_1567_eslf.png',  'IMG_1583_eslf.png',  'IMG_1599_eslf.png','IMG_0359_eslf.png',  'IMG_0575_eslf.png',  'IMG_0820_eslf.png',  'IMG_1414_eslf.png' ,  'IMG_1469_eslf.png' , 'IMG_1474_eslf.png' , 'IMG_1478_eslf.png' , 'IMG_1482_eslf.png' , 'IMG_1487_eslf.png' , 'IMG_1501_eslf.png' , 'IMG_1509_eslf.png' , 'IMG_1514_eslf.png' ,'IMG_1527_eslf.png', 'IMG_1538_eslf.png', 'IMG_1560_eslf.png', 'IMG_1568_eslf.png',  'IMG_1594_eslf.png' , 'IMG_1600_eslf.png','IMG_0360_eslf.png',  'IMG_0596_eslf.png' , 'IMG_1016_eslf.png' , 'IMG_1415_eslf.png' , 'IMG_1470_eslf.png'  ,'IMG_1475_eslf.png' , 'IMG_1479_eslf.png' , 'IMG_1483_eslf.png' , 'IMG_1490_eslf.png' , 'IMG_1504_eslf.png' , 'IMG_1510_eslf.png' , 'IMG_1516_eslf.png' , 'IMG_1529_eslf.png' , 'IMG_1544_eslf.png' , 'IMG_1565_eslf.png' , 'IMG_1580_eslf.png' , 'IMG_1595_eslf.png' , 'IMG_1601_eslf.png' ]

    trainingLightFieldFilenames = ['fishi_5x5_ap35', 'messerschmitt_5x5_ap35', 'shrubbery_5x5_ap35', 'dice_5x5_ap35']
    
    for index,filename in enumerate(trainingLightFieldFilenames): 
        data_input = load_lf_to_tensor_syn(filename)
        TT = data_input[0]
        numTrainingLightFields = data_input[1]
    
        # number of training patches
        numTrainingPatches = Config.numTrainingPatches;
        lightFieldRes = Config.lightFieldRes
    
        # size of a patch [spatial_y spatial_x angular_y angular_x]
        if Flags.neighbor == 0:
            patchSize = Config.patchSize
        else:
            patchSize = Config.patchSize_neighbor
    
        ############################################################
        # Preparing block diagonal Phi
        ############################################################
        if Flags.diagPhi == 1:
            patchsizeDiag = (5, 5, 1, 1)
            num = 6
            phiBlock = np.zeros((2*num,patchsizeDiag[0]*patchsizeDiag[1]))
            for i in xrange(num):
                yi = i/6 #since both are integers, divide = floor
                xi = np.mod(i,6)
                phiBlock[2*i:2*i+2,:] = formPhi_func(patchsizeDiag,yi,xi,1)
    
            #print phiBlock.shape
            #hinton.hinton(phiBlock,"phiBlock")

            #hinton.hinton(normalize(phiBlock),"phiBlock")
            #print normalize(phiBlock)
    
            # Make block diagonal
            Phi = np.kron(np.eye(patchSize[0]*patchSize[1]), phiBlock)
            #print Phi.shape
            #hinton.hinton(Phi,"phiBlock")
        else:
            num = 1
    
        ############################################################
        # now get random patches and simulate measurements on sensor
        if Flags.create_data == 1:
                    # number of Measurements for each patch
            #if Flags.interpolate == False:
            #M = 2*num*patchSize[0]*patchSize[1]
            M = N*num*patchSize[0]*patchSize[1]
            #else:
            M_inter = np.prod(patchSize)
    
        
            Patches = np.zeros((numTrainingPatches,)+(1,)+patchSize)
            #disparity = np.zeros((numTrainingPatches,)+(1,)+patchSize)
            mod_patch = np.zeros((numTrainingPatches,patchSize[0]*patchSize[2],patchSize[1]*patchSize[3]))
            # Measurements : [numTrainingPatches, nColor, numMeasurements per patch]
            Measurements = np.zeros((numTrainingPatches,1,M))
            #mask_measurements = np.zeros((numTrainingPatches,1,M))
            Measurements_inter = np.zeros((numTrainingPatches,1,M_inter))
            # Seed to reproduce(change this out if different results required)
            sd1 = 20
            np.random.seed(sd1)
            status = 0
            #for k in range(numTrainingPatches):
            k = 0
            while k < numTrainingPatches:
                if k >= status:
                    print "Iteration: ", k
                    status += 1000
                #N = np.random.randint(0,numTrainingLightFields) #pick which LF
                y = np.random.randint(0,lightFieldRes[0]-patchSize[0]) #pick y
                x = np.random.randint(0,lightFieldRes[1]-patchSize[1]) #pick x
                Patches[k,0,:,:,:,:] = TT[0,0,y:y+patchSize[0],x:x+patchSize[1],:,:]
                vecPatch = np.ravel(Patches[k,0,:,:,:,:])
                
                #for index1 in range(patchSize[0]):
                #    for index2 in range(patchSize[1]):
                #        mod_patch[k,index1*patchSize[2]:index1*patchSize[2]+patchSize[2],index2*patchSize[3]:index2*patchSize[3]+patchSize[3]] = Patches[k,0,index1,index2,:,:]
                
                #vecPatch = np.ravel(mod_patch[k,:,:])
                # Get the phi corresponding to this patch defined by y and x
                if Flags.diagPhi == 0:
                    Phi = formPhi_func(patchSize,y,x,1)
                #if Flags.normalizePhi == 1:
                #    Phi = normalize(Phi)
                # simulate measurement on sensor
                #if Flags.interpolate == False:
                if mask_phi_flag ==1:
                    Phi = mask_phi(x,y)
                
                Measurements[k,0,:] =  np.dot(Phi,vecPatch)
                #Measurements[k,0,:] = np.dot(Phi,vecPatch)
                #else:
                Measurements_inter[k,0,:] = np.dot(Phi.T,np.dot(Phi,vecPatch))
                 
                # subtract mean
                if Flags.subMean == 1:
                    Measurements[k,0,:] = Measurements[k,0,:] - np.mean(Measurements[k,0,:])
                k += 1
            '''
            l=0
            Patches=[]
    	    Measurements=[]
            i=0
            j=0
            stride = Config.stride
            numTrainingPatches = (int((lightFieldRes[0]-patchSize[0])/stride[0])+1)*(int((lightFieldRes[1]-patchSize[1])/stride[1]+1))*numTrainingLightFields
            Measurements = np.zeros((numTrainingPatches,1,M))
            Patches = np.zeros((numTrainingPatches,)+(1,)+patchSize)  # resulting in matrix of size (k,1,60,60,60,5,5)
            for k in range(numTrainingLightFields):
                print "Light Field: ", k 
                while i <= lightFieldRes[0]-patchSize[0]:
                    while j <= lightFieldRes[1]-patchSize[1]:
                        Patches[l,0,:,:,:,:] = TT[k,0,i:i+patchSize[0],j:j+patchSize[1],:,:]
                        l+=1
                        j=j+stride[1]
                        vecPatch = np.ravel(Patches[l,0,:,:,:,:])
                        if Flags.diagPhi == 0:
                            Phi = formPhi_func(patchSize,i,j,1)
                        if Flags.interpolate == False:
                            Measurements[l,0,:] = np.dot(Phi,vecPatch)
                        else:
                            Measurements[l,0,:] = np.dot(Phi.T,np.dot(Phi,vecPatch))
    	            #import pdb
                        #pdb.set_trace()
                    i = i+ stride[0]
            '''
            print "Measurements shape: " , Measurements.shape
            print 'Phi shape: ', Phi.shape
    
            # split into train and validation (must be called data, label)
            if Flags.neighbor == 0:
                patchSizeh5py = patchSize
                sx_start = 0
                sx_end = 9
                sy_start = 0
                sy_end = 9
            else:
                patchSizeh5py = (11,11,4,4)
                sx_start = 2
                sx_end = 13
                sy_start = 2
                sy_end = 13
    
            splitPer = 0.85
            split = int(np.floor(numTrainingPatches*splitPer))
    
            label = np.zeros((split,1,np.prod(patchSizeh5py)),dtype='float64')
    	    label1 = np.zeros((split,1,1),dtype='float64')
            label_val = np.zeros((numTrainingPatches-split,1,np.prod(patchSizeh5py)),dtype='float64')
    	    label1_val = np.zeros((numTrainingPatches-split,1,1),dtype='float64')
    
            data = np.zeros((split,1,M),dtype='float64')
            data_phit = np.zeros((split,1,M_inter),dtype='float64')
            data_val = np.zeros((numTrainingPatches - split,1,M),dtype='float64')
            data_val_phit = np.zeros((numTrainingPatches - split,1,M_inter),dtype='float64')

            # size tests
            print 'trainlabelshape = ' +str(label.shape)
            print 'vallabelshape = ' + str(label_val.shape)
    
            for k in range(0,numTrainingPatches):
                if k >= split:
                    data_val[k-split,0,:] = Measurements[k,0,:]
                    data_val_phit[k-split,0,:] = Measurements_inter[k,0,:]
                    if Flags.weighted == True:
                        label_val[k-split,0,:] = wl.weighted_loss(np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:]),patchSize)
                    else:
                        label_val[k-split,0,:] = np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:])
                        #label_val[k-split,0,:] = np.ravel(mod_patch[k,:,:])
                else:
                    data[k,0,:] = Measurements[k,0,:]
                    data_phit[k-split,0,:] = Measurements_inter[k,0,:]
                    if Flags.weighted == True:
                        label[k,0,:] = wl.weighted_loss(np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:]),patchSize)
                    else:
                        label[k,0,:] = np.ravel(Patches[k,0,sx_start:sx_end,sy_start:sy_end,:,:])
                        #label[k,0,:] = np.ravel(mod_patch[k,:,:])
            #create hdf5 formats compatible with Caffe
    	    if Flags.interpolate == False:
            	tfn = "maskphi/train-ASPphi_syn_mask_N=%d_phit_%s-=%d-%d-%d.h5"%(N,phitype,num,numTrainingPatches,index)
            	vfn = "maskphi/val-ASPphi_syn_mask_N=%d_phit_%s-=%d-%d-%d.h5"%(N,phitype,num,numTrainingPatches,index)
            else:
    		tfn = "maskphi/train-ASPphi_syn_mask_N=%d_phit_%s-=%d-%d-interpolate-%d.h5"%(N,phitype,num,numTrainingPatches,index)
                vfn = "maskphi/val-ASPphi_syn_mask_N=%d_phit_%s-=%d-%d-interpolate-%d.h5"%(N,phitype,num,numTrainingPatches,index)
            
            #from write_lmdb import gen_data
            #gen_data(data,label,label1,tfn)
    	    f  = h5py.File(tfn,"w")
            f['dataphit'] = data_phit.astype('float64')
            f['data'] = data.astype('float64')
            f['label'] = label.astype('float64')
    	    f['label1'] = label1.astype('float64')
            f.close()
            
            #gen_data(data_val,label_val,label1_val,vfn)
            f2 = h5py.File(vfn,"w")
            f2['dataphit'] = data_val_phit.astype('float64')
            f2['data'] = data_val.astype('float64')
            f2['label'] = label_val.astype('float64')
            f2['label1'] = label1_val.astype('float64')
    	    f2.close()
            print "Dumped: %s, %s at %s" % (tfn,vfn,os.getcwd())
    
            # Dump weightl2 in h5 format
            if Flags.weighted == True:
                wl.dump_weight_h5(patchSize)
        else:
                print 'Enable data_create flag to generate train/val h5 files'


import formPhi as fm

if __name__ == "__main__":
    create_training_data(fm.formPhi, 'Sim')