import numpy as np
import ASPProjMatrixTwoPhase as aspm
from flags_config import Config
import os
import scipy.io as sio

def load_angular_response_real():
    print "***********Reading angular response***********"
    # Get config
    lightFieldRes = Config.lightFieldRes
    lightFieldAngularResponseRes = Config.lightFieldAngularResponseRes

    # Pre-allocating
    lightFieldAngularResponse = np.zeros(lightFieldAngularResponseRes)
    lightFieldAngularResponseDual = np.zeros(lightFieldAngularResponseRes)

    for ky in range(lightFieldRes[2]):
        for kx in range(lightFieldRes[3]):
            # Load the angular response to form Real Phi
            datapath_calibration = os.path.dirname(os.path.realpath(__file__)) + "/../ASP Reconstruction Pipeline/DATA/Calibration Datasets/White_PigImpulse/"
            filename_calibration = 'ImpulseCal_';
            angularData = sio.loadmat(datapath_calibration+filename_calibration+'%02d'%((lightFieldRes[2]-ky)*10+(kx+1))+'.mat')
            lightFieldAngularResponse[:,:,ky,kx] = angularData['im1']
            lightFieldAngularResponseDual[:,:,ky,kx] = angularData['im2']

    # repeat angular response to capture full resolution of lightField
    lightFieldAngularResponse = np.tile(lightFieldAngularResponse, (np.ceil(lightFieldRes[0]/float(lightFieldAngularResponseRes[0])),np.ceil(lightFieldRes[1]/float(lightFieldAngularResponseRes[1])),1,1))
    lightFieldAngularResponseDual = np.tile(lightFieldAngularResponseDual, (np.ceil(lightFieldRes[0]/float(lightFieldAngularResponseRes[0])),np.ceil(lightFieldRes[1]/float(lightFieldAngularResponseRes[1])),1,1))

    return [lightFieldAngularResponse, lightFieldAngularResponseDual]

def formPhi(patchSize,sy,sx, bRecoverDiff=1):
    """ Function to build Phi matrix
        Input:
        patchSize: [spatialy,spatialx,angulary,angularx]
        sy: y pos of top left corner of patch
        sx: x pos of top left corner of patch
        bRecoverDiff: differential mode on/off
    """

    # Re-organize: patchSizefm: [angulary,angularx,spatialy,spatialx]
    # Note: This reorganize is needed for aspm function call
    patchSizefm = (patchSize[2], patchSize[3], patchSize[0], patchSize[1])

    #R1,R2 = aspm.ASPProjMatrixTwoPhase(patchSize,sy*patchSize[2],sx*patchSize[3])
    R1,R2 = aspm.ASPProjMatrixTwoPhase(patchSizefm,sy,sx)

    # ravel R1 and R2 into a row vector
    R1p = np.ravel(R1,order='F')
    R2p = np.ravel(R2,order='F')

    # Local variables
    numAP = patchSizefm[0]*patchSizefm[1]   # number of angular pixels
    numSP = patchSizefm[2]*patchSizefm[3]   # number of spatial pixels

    PHI = np.zeros((2*numSP, np.prod(patchSizefm)))

    count = 0
    if bRecoverDiff == 1:
        # Differential
        for kk in range(numSP):
            PHI[kk, count:count+numAP] = np.squeeze(R2p[count:count+numAP] - R1p[count:count+numAP])
            PHI[kk+numSP,count:count+numAP] =  np.squeeze((R2p[count:count+numAP] + R1p[count:count+numAP])/2.)
            count = count+numAP
    else:
        # Dual mode
        for kk in range(numSP):
            PHI[kk, count:count+numAP]       = R1p[count:count+numAP]
            PHI[kk+numSP, count:count+numAP] = R2p[count:count+numAP]
            count = count + numAP
    #print "Shape of PHI: " , PHI.shape
    
    return PHI

#def rotate(PHI):
#    phi = []
#    for row in PHI:
#        np.reshape(



def formPhiReal(patchSize,sy,sx, bRecoverDiff=1):
    """ Function to build Phi matrix
        Input:
        patchSize: [spatialy,spatialx,angulary,angularx]
        sy: y pos of top left corner of patch
        sx: x pos of top left corner of patch
        bRecoverDiff: differential mode on/off
    """
    if 'lightFieldAngularResponse' not in globals():
        global lightFieldAngularResponse
        global lightFieldAngularResponseDual
        lightFieldAngularResponse, lightFieldAngularResponseDual = load_angular_response_real()
    # Get the phi corresponding to this patch defined by sy and sx
    R1 = lightFieldAngularResponse[sy:sy+patchSize[0],sx:sx+patchSize[1],:,:]
    R2 = lightFieldAngularResponseDual[sy:sy+patchSize[0],sx:sx+patchSize[1],:,:]

    # ravel R1 and R2 into a row vector
    R1p = np.ravel(R1,order='F')
    R2p = np.ravel(R2,order='F')

    # Local variables
    numAP = patchSize[2]*patchSize[3]   # number of angular pixels
    numSP = patchSize[0]*patchSize[1]   # number of spatial pixels

    PHI = np.zeros((2*numSP, np.prod(patchSize)))

    count = 0
    if bRecoverDiff == 1:
        # Differential
        for kk in range(numSP):
            PHI[kk, count:count+numAP] = np.squeeze(R2p[count:count+numAP] - R1p[count:count+numAP])
            PHI[kk+numSP,count:count+numAP] =  np.squeeze((R2p[count:count+numAP] + R1p[count:count+numAP])/2.)
            count = count+numAP
    else:
        # Dual mode
        for kk in range(numSP):
            PHI[kk, count:count+numAP]       = R1p[count:count+numAP]
            PHI[kk+numSP, count:count+numAP] = R2p[count:count+numAP]
            count = count + numAP
    #print "Shape of PHI: " , PHI.shape

    return PHI


## Mask Phi code also to be added here from get_training_data file
