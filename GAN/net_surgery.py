"""
Author: Mayank
6 Oct, 2016

This file updates the weights for generator model using weights from discriminator model
"""

import numpy as np
import caffe
import os

path = "examples/light-field/UCSD/GAN/"

model_gen = path+ 'deploy_interpolated_generator.prototxt'
model_disc= path+ 'deploy_interpolated_discriminator.prototxt'
weight_gen = path+ 'gen_iter_4.caffemodel'
weight_disc = path+ 'disc_iter_8.caffemodel'

caffe.set_mode_gpu()
caffe.set_device(0)

net_gen = caffe.Net(model_gen, weight_gen, caffe.TEST)
net_disc = caffe.Net(model_disc,weight_disc, caffe.TEST)

layers2 = ['conv1', 'conv2']
layers = ['disc0_1', 'disc1_1', 'disc2_1']

target_params = {pr: (net_gen.params[pr][0].data, net_gen.params[pr][1].data) for pr in layers}
source_params = {pr: (net_disc.params[pr][0].data, net_disc.params[pr][1].data) for pr in layers}

for pr in layers:
	target_params[pr][1][...] = source_params[pr][1]
	target_params[pr][0][...] = source_params[pr][0]

os.remove(weight_gen)
net_gen.save(weight_gen)

'''
for layer in layers:
	for i in range(len(net_gen.params[layer][0].data)):
		net_gen.params[layer][0].data[i] = net_disc.params[layer][0].data[i]
	for j in range(len(net_gen.params[layer][1].data)):
		net_gen.params[layer][1].data[j] = net_disc.params[layer][1].data[j]

for layer in layers2:
	for i in range(len(net_gen.params[layer][0].data)):
		net_gen.params[layer][0].data[i] = net_gen.params[layer][0].data[i]
	for j in range(len(net_gen.params[layer][1].data)):
		net_gen.params[layer][1].data[j] = net_gen.params[layer][1].data[j]
'''