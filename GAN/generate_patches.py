'''
Author: Mayank

6 Oct 2016

File to generate test and training data for discriminator after every couple of iterations of generator

'''

import numpy as np
import caffe
import h5py , pdb
import random


def shuffle(array):
    size=len(array)
    index = range(size)
    random.shuffle(index)
    tmp=[]
    for i in index:
        tmp.append(array[i])
    return tmp


Model_file = 'examples/light-field/2stream/GAN/deploy_generator.prototxt'
Weights = 'examples/light-field/2stream/GAN/gen_iter_4.caffemodel'

File_path = '../light-field_old/ASPSimulator/'
file_index= range(100)
random.shuffle(file_index)

train_string = 'train-ASPphi_UCSD_phit_Sim-N=1-5000-'
test_string = 'val-ASPphi_UCSD_phit_Sim-N=1-5000-'

file_1 = File_path+ train_string+str(file_index[0])+'.h5'
file_2 = File_path+ train_string+str(file_index[1])+'.h5'
random.shuffle(file_index)
file_11 = File_path+ train_string+str(file_index[2])+'.h5'
file_21 = File_path+ train_string+str(file_index[3])+'.h5'
random.shuffle(file_index)
file_3 =  File_path+ test_string+str(file_index[2])+'.h5'
file_4 =  File_path+ test_string+str(file_index[3])+'.h5'
random.shuffle(file_index)
file_31 =  File_path+ test_string+str(file_index[6])+'.h5'
file_41 =  File_path+ test_string+str(file_index[7])+'.h5'

caffe.set_device(0)
caffe.set_mode_gpu()

print " Starting generating patches for discriminator \n "

# Extract test and validataion data with label 0
train_f1 = h5py.File(file_1,"r")
train_f2 = h5py.File(file_2,"r")
test_f1 = h5py.File(file_3,"r")
test_f2 = h5py.File(file_4,"r")

train_f1_data = shuffle(train_f1['data'])
train_f2_data = shuffle(train_f2['data'])

test_f1_data = shuffle(test_f1['data'])
test_f2_data = shuffle(test_f2['data'])

train_input_data = np.concatenate((train_f1_data[:500],train_f2_data[:500]))
test_input_data = np.concatenate((test_f1_data[:500],test_f2_data[:500]))

net = caffe.Net(Model_file, Weights, caffe.TEST)
net.blobs['data'].data[...] = np.reshape(train_input_data,(1000,1,162,1))
net.forward()
train_data_tmp = net.blobs['r2'].data

net2 = caffe.Net(Model_file, Weights, caffe.TRAIN)
net2.blobs['data'].data[...] = np.reshape(test_input_data,(1000,1,162,1))
net.forward()
test_data_tmp = net2.blobs['r2'].data

train_data0 = np.reshape(train_data_tmp,(1000,1,2025))
test_data0 = np.reshape(test_data_tmp[:250],(250,1,2025))


# Extract test and validation data with label 1
train_f1 = h5py.File(file_11,"r")
train_f2 = h5py.File(file_21,"r")
test_f1 = h5py.File(file_31,"r")
test_f2 = h5py.File(file_41,"r")

train_f1_data = shuffle(train_f1['label'])
train_f2_data = shuffle(train_f2['label'])

test_f1_data = shuffle(test_f1['label'])
test_f2_data = shuffle(test_f2['label'])

train_data1 = np.reshape(np.concatenate((train_f1_data[:500],train_f2_data[:500])),(1000,1,2025))
test_data1 = np.reshape(np.concatenate((test_f1_data[:125],test_f2_data[:125])),(250,1,2025))


# Combine all the data and labels
train_data = np.concatenate((train_data1,train_data0))
train_label = np.concatenate((np.ones((1000,1)),np.zeros((1000,1))))
test_data = np.concatenate((test_data1,test_data0))
test_label = np.concatenate((np.ones((250,1)),np.zeros((250,1))))

'''
Shuffle training data 
'''
# --------------------------------------

index_shuf = range(2000)
random.shuffle(index_shuf)

train_data_shuffled=[]
train_label_shuffled=[]

count = 0
for i in index_shuf:
	train_data_shuffled.append(train_data[i])
	train_label_shuffled.append(train_label[i])

# --------------------------------------	
# Train data = 2000 , Test data = 500

f1 = h5py.File("examples/light-field/2stream/GAN/Train_data.h5","w")
f2 = h5py.File("examples/light-field/2stream/GAN/Test_data.h5","w")

f1['data'] = np.asarray(train_data_shuffled).astype('float64')
f1['label'] = np.asarray(train_label_shuffled).astype('float64')
f1.close()

f2['data'] = np.asarray(test_data).astype('float64')
f2['label'] = np.asarray(test_label).astype('float64')
f2.close()



print " Patches for discriminator generated \n"
