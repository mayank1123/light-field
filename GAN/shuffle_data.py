'''
Author: Mayank
6 Oct 2016
File to generate test and training data for the generator
'''

import numpy as np
import caffe
import h5py , pdb
import random



def shuffle(array1,array2,array3,size):
    size=len(array1)
    index = range(size)
    random.shuffle(index)
    tmp1=[]
    tmp2=[]
    tmp3=[]
    for i in index[:size]:
        tmp1.append(array1[i])
        tmp2.append(array2[i])
        tmp3.append(array3[i])
    return tmp1, tmp2 , tmp3

File_path = '../light-field_old/ASPSimulator/'
File_index= range(100)

print "Shuffling data \n" 
# generate train data
random.shuffle(File_index)
file_tmp = File_path+ 'train-ASPphi_UCSD_phit_Sim-N=1-5000-'+str(File_index[0])+'.h5'
h5File = h5py.File(file_tmp,"r")

data = h5File['data']
label = h5File['label']
label1 = h5File['label1']

data_shuffled, label_shuffled, label1_shuffled = shuffle(data,label,label1,2000)
f1 = h5py.File("examples/light-field/2stream/GAN/Train_data_gen.h5","w") 
f1['data'] = np.asarray(data_shuffled).astype('float64')
f1['label'] = np.asarray(label_shuffled).astype('float64')
f1['label1'] = np.asarray(label1_shuffled).astype('float32')
f1.close()

# generate test data
random.shuffle(File_index)
file_tmp = File_path+ 'val-ASPphi_UCSD_phit_Sim-N=1-5000-'+str(File_index[0])+'.h5'
h5File = h5py.File(file_tmp,"r")

data = h5File['data']
label = h5File['label']
label1 = h5File['label1']

data_shuffled, label_shuffled, label1_shuffled = shuffle(data,label,label1,200)
f1 = h5py.File("examples/light-field/2stream/GAN/Test_data_gen.h5","w")          
f1['data'] = np.asarray(data_shuffled).astype('float64')
f1['label'] = np.asarray(label_shuffled).astype('float64')
f1['label1'] = np.asarray(label1_shuffled).astype('float32')
f1.close()

print "Shuffling Done \n"
