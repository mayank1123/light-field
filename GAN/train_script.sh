#!/bin/bash

# Script to train GANs on UCSD light-field data
# shuffle_data.py : shuffle and create data file for generator
# generate_patches.py : generate patches for discriminator 

#rm examples/light-field/UCSD/GAN/Log.txt
PATH_1="examples/light-field/2stream/GAN"
gen_solver="examples/light-field/2stream/GAN/generator_solver.prototxt"
disc_solver="examples/light-field/2stream/GAN/discriminator_solver.prototxt"
gen_weights="$PATH_1/gen_iter_4.caffemodel"
disc_weights="$PATH_1/disc_iter_8.caffemodel"

#python shuffle_data.py
#./build/tools/caffe train --solver=$gen_solver --weights=$gen_weights -gpu 0 2>&1 | tee $PATH_1/Log.txt 
#python generate_patches.py 2>&1 | tee -a $PATH_1/Log.txt
#./build/tools/caffe train --solver=$disc_solver --weights=$disc_weights -gpu 0 2>&1 | tee -a $PATH_1/Log.txt
#python net_surgery.py 2>&1 | tee -a $PATH_1/Log.txt

for i in {20000..200000} ; do
	
	echo " \n -------------------------------------------------------------- Iteration $i -------------------------------------------- \n" 2>&1 | tee -a $PATH_1/Log.txt 
	
	python shuffle_data.py 2>&1 | tee -a $PATH_1/Log.txt
	echo "Next \n"
	./build/tools/caffe train --solver=$gen_solver --weights=$gen_weights -gpu 0 2>&1 | tee -a $PATH_1/Log.txt
	python generate_patches.py 2>&1 | tee -a $PATH_1/Log.txt
	./build/tools/caffe train --solver=$disc_solver --weights=$gen_weights -gpu 0 2>&1 | tee -a $PATH_1/Log.txt 
	
	if (( $i % 500 ==0 ))
	then
	
	cp $gen_weights $PATH_1/LFReconNet_gen_iter_$(($i)).caffemodel	
	fi
done