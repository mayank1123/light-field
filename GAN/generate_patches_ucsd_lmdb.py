'''
Author: Mayank

6 Oct 2016

File to generate test and training data for the discriminator after every 170 iterations of generator

'''

import numpy as np
import caffe
import h5py , pdb
import random , lmdb
import re, fileinput, math


def save_to_lmdb(file_name, data, label, start_index):
    data_file = ''
    label_file = ''
    for idx in range(int(math.ceil(len(label)/1000.0))):
        in_label = lmdb.open(label_file,map_size=int(1e12))
        with in_label.begin(write=True) as in_txn:
            for label_idx, label_ in enumerate(label[1000*idx:1000(idx+1)):
                im_dat = caffe.io.array_to_datum(np.array(label_).astype(float))
                in_txn.put('{:0>10d}'.format(start_index+1000*idx+label_idx), im_dat.SerializeToString())

                string_ = str(1000*idx+label_idx+1) +' / ' + str(len(label))
                sys.stdout.write("\r%s" % string_)
                sys.stdout.flush()
        in_db_label.close()
    # to append to the same file for next time
    #return idx*1000+label_idx+start_index+1

    for idx in range(int(math.ceil(len(data)/1000.0))):
        in_data = lmdb.open(data_file,map_size=int(1e12))
        with in_data.begin(write=True) as in_txn:
            for data_idx, data_ in enumerate(data[1000*idx:1000(idx+1)):
                im_dat = caffe.io.array_to_datum(np.array(data_).astype(float))
                in_txn.put('{:0>10d}'.format(start_index+1000*idx+label_idx), im_dat.SerializeToString())

                string_ = str(1000*idx+label_idx+1) +' / ' + str(len(label))
                sys.stdout.write("\r%s" % string_)
                sys.stdout.flush()
        in_db_label.close()


batch_size =250
total_patches = 5000

Model_file = 'examples/light-field/UCSD/GAN/deploy_interpolated.prototxt'
Weights = 'examples/light-field/UCSD/UCSD_data_LF_interpolated_reconstruction_iter_850.caffemodel'
#Weights = 'ASP_Sim_N1-6fc-10patch2-lr1e-6_iter_5780.caffemodel'
#Weights = 'examples/light-field/LFReconNet_gen_fine_tuned_100000_disc_680_itr_iter_36720.caffemodel'

caffe.set_device(1)
caffe.set_mode_gpu()

index_shuf=range(100)

for i in index_shuff[:20]:
    # Setup caffe.net with appropriate data inputs
    train_f = h5py.File("../light-field/ASPSimulator/train-ASPphi_UCSD_Real-N=1-5000-interpolate-94.h5","r")
    train_input_data = train_f['data']
    
    test_f = h5py.File("../light-field/ASPSimulator/val-ASPphi_UCSD_Real-N=1-5000-interpolate-94.h5","r")
    test_input_data = test_f['data']
    
    #pdb.set_trace()
    # Extract test and validataion data with label 0
    net = caffe.Net(Model_file, Weights, caffe.TEST)
    net.blobs['data'].data[...] = np.reshape(train_input_data[0:batch_size],(batch_size,1,3025,1))
    net.forward()
    data = net.blobs['r2'].data
    
    #pdb.set_trace()
    # Batch size is 250
    for i in range(1,17):
    	print "Iteration %d \n"%(i)
    	net.blobs['data'].data[...] = np.reshape(train_input_data[i*batch_size:(i+1)*batch_size],(batch_size,1,3025,1))
    	net.forward()
    	data = np.concatenate((data, net.blobs['r2'].data))
    
    #pdb.set_trace()
    #data = np.reshape(data1,(42500,1,3025))
    
    net2 = caffe.Net(Model_file, Weights, caffe.TRAIN)
    net2.blobs['data'].data[...] = np.reshape(test_input_data[0:batch_size],(batch_size,1,3025,1))
    net.forward()
    data = np.concatenate((data,net2.blobs['r2'].data))
    
    for i in range(1,3):
    	print "Iteration test %d \n"%(i)
    	net2.blobs['data'].data[...] = np.reshape(test_input_data[i*batch_size:(i+1)*batch_size],(batch_size,1,3025,1))
    	net.forward()
    	data = np.concatenate((data,net2.blobs['r2'].data))
    
    data = np.reshape(data,(5000,1,3025))
    label = np.zeros((5000,1),dtype='float64')
    #random.shuffle(data)
    
    data1 = data  # Randomly selected training and test data with label 0
    data2 = data  
    
    label1 = np.zeros((4250,1),dtype='float64')
    label2 = np.zeros((750,1),dtype='float64')
    
    # Extract test and validation data with label 1
    
    #f3 = h5py.File("examples/light-field/ASPSimulator/train-dis-ASPphi-N=1-20-50000.h5","r")
    #data_ = train_f['label']#[0:5000]
    #label3 = np.ones((42500,1),dtype='float64')#[0:5000]
    
    #f4 = h5py.File("examples/light-field/ASPSimulator/val-dis-ASPphi-N=1-20-50000.h5","r")
    #data_ = np.concatenate((data_,test_f['label']))#[0:500]
    #random.shuffle(data_)
    
    data3 = train_f['label'] #data_[:42500,:,:] # Randomly selected other half od training and test data with label 1
    data4 = test_f['label'] #data_[42500:,:,:]
    
    label3 = np.ones((4250,1),dtype='float64')
    label4 = np.ones((750,1),dtype='float64')#[0:500]
        
    '''
    Shuffle training data 
    '''
    # --------------------------------------
    
    data_unshuffled = np.concatenate((data1,data3))
    label_unshuffled = np.concatenate((label1,label3))
    
    index_shuf = range(8500)
    random.shuffle(index_shuf)
    
    data_train=[]
    label_train=[]
    
    count = 0
    for i in index_shuf:
    	data_train.append(data_unshuffled[i])
    	label_train.append(label_unshuffled[i])
    
    # --------------------------------------    		
    
    data_test = np.concatenate((data2,data4))
    label_test = np.concatenate((label2,label4))
    
    #import re
    #itr = np.int(re.search( 'iter_(.*).caffe', Weights).group(1))
    
    #f1 = h5py.File("train_disc_only_rand-%d.h5"(itr),"w")
    #f2 = h5py.File("test_disc_only_rand-%d.h5"(itr),"w")
    
    f1 = h5py.File("examples/light-field/UCSD/GAN/Disc_train_data_shuffled.h5","w")
    f2 = h5py.File("examples/light-field/UCSD/GAN/Disc_test_data_shuffled.h5","w")
    
    f1['data'] = np.asarray(data_train).astype('float64')
    f1['label'] = np.asarray(label_train).astype('float64')
    f1.close()
    
    f2['data'] = np.asarray(data_test).astype('float64')
    f2['label'] = np.asarray(label_test).astype('float64')
    f2.close()
