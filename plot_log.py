import re
from matplotlib import pyplot as plt


log_file='examples/light-field/UCSD/GAN/Log.txt'
file_ = open(log_file,'r')
loss=[]

for line in file_:
    try:
        if 'Test' in line:
            loss.append(float(re.search('loss = (.*) \(',line).group(1)))
    except:
        pass

plt.plot(loss)
plt.show()
#print loss